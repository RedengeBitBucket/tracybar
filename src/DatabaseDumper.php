<?php

namespace Redenge;

use Tracy;

require(__DIR__ . '/Dumper.php');

/**
 * @todo
 * Link do editoru
 * Prostor pro dumple dotazy + schovat všechny
 * Zkopirovat SQL do schránky => pustit
 */
final class DatabaseDumper implements Tracy\IBarPanel
{
	private $adminer_url;

	private $time;

	private $db_host, $db_username, $db_name;
	
	private $columns;
	
	private $data;
	
	private $count;
	
	private $sql_length;
	
	public function __construct($columns, $data)
	{
		$this->adminer_url = FALSE;
		
		$this->columns = $columns;
		$this->data = $data;
		$this->count = count($data);
		$this->sql_length = 200;
	}
	
	public function setSqlLength($length)
	{
		$this->sql_length = $length;
	}
	
	/**
	 * Přidá odkaz na adminera, kde se naklonuje konkrétní dotaz
	 * @param boolean $url
	 */
	public function setAdminerUrl($url)
	{
		$this->adminer_url = $url;
	}

	public function setDBDetails($db_host, $db_username, $db_name)
	{
		$this->db_host = $db_host;
		$this->db_username = $db_username;
		$this->db_name = $db_name;
	}
	
	
	/**
	 * Nastavení doby vykonávání sql dotazů
	 * @param float $time - čas v sekundách
	 */
	public function setTime($time)
	{
		$this->time = round($time * 1000, 2); // ms
	}

	public function getTab()
	{
		/*
		 * Barva ikonky
		 */
		if ($this->time < 100)
			$color = '009933';
		else if ($this->time < 1000)
			$color = '0033ff';
		else
			$color = 'ff3333';

		return "<span title=\"Databáze\">".
'<svg viewBox="0 0 2048 2048"><path fill="#'.$color.'" d="M1024 896q237 0 443-43t325-127v170q0 69-103 128t-280 93.5-385 34.5-385-34.5-280-93.5-103-128v-170q119 84 325 127t443 43zm0 768q237 0 443-43t325-127v170q0 69-103 128t-280 93.5-385 34.5-385-34.5-280-93.5-103-128v-170q119 84 325 127t443 43zm0-384q237 0 443-43t325-127v170q0 69-103 128t-280 93.5-385 34.5-385-34.5-280-93.5-103-128v-170q119 84 325 127t443 43zm0-1152q208 0 385 34.5t280 93.5 103 128v128q0 69-103 128t-280 93.5-385 34.5-385-34.5-280-93.5-103-128v-128q0-69 103-128t280-93.5 385-34.5z"></path></svg>'.
"{$this->time} ms / {$this->count}
</span>";
	}


	public function getPanel()
	{	
		$html = sprintf('<h1 title="%s">Queries: %d, time: %s ms</h1>', 'mysql:host=127.0.0.1;database=eta', $this->count, $this->time ) .";

		<div class=\"tracy-inner nette-DbConnectionPanel\">
			<table>
				<tbody>
					<tr>";
		
		if ($this->adminer_url) {
			$html .= "<th>Run</th>";
		}
		
		foreach($this->columns as $column)
		{
			$html .= "<th>" .$column . "</th>";
		}
		
		
		$html .= "</tr>";

		foreach ($this->data as $data) {

			if ($this->adminer_url) {
				$sql = isset($data[2]) ? $data[2] : '';
				
				$link = 'http://' . $this->adminer_url . '/?username=' . $this->db_username . '&db=' . $this->db_name . '&sql=' . urlencode($sql);
				$form = '<form action="'.$link.'" method="post" ><input type="submit" value="►" /></form>';
				$anchor = "<a href=\"{$link}\" target=\"_blank\" title=\"Spustit dotaz\">►</a> ";
			} else {
				$anchor = '';
			}

			$html .= "<tr>";
			
			if ($this->adminer_url) 
			{
				$html .= "<td>{$anchor}</td>";
			}
				
			
			for($i = 0 ; $i < count($this->columns) ; $i++)
			{
				$text = isset($data[$i]) ? $data[$i] : '';
				
				// posledni polozka by mela byt sql dotaz, tak dotaz vykreslime ,,pekneji"
				if($i == count($this->columns) - 1)
				{
					$html .= "<td class=\"nette-DbConnectionPanel-sql\">" . $this->dumpSql(preg_replace('/\s+/', ' ', $text))."</td>";
				}
				else
				{
					$html .= "<td>{$text}</td>";
				}
			}
			
			$html .= "</tr>";
				
			
		}

		$html .= "</tbody>
	</table>
</div>";

		return $html;
	}
	
	/**
	 * Returns syntax highlighted SQL command.
	 * @param  string
	 * @return string
	 */
	private function dumpSql($sql, array $params = NULL, Connection $connection = NULL)
	{
		static $keywords1 = 'SELECT|(?:ON\s+DUPLICATE\s+KEY)?UPDATE|INSERT(?:\s+INTO)?|REPLACE(?:\s+INTO)?|DELETE|CALL|UNION|FROM|WHERE|HAVING|GROUP\s+BY|ORDER\s+BY|LIMIT|OFFSET|SET|VALUES|LEFT\s+JOIN|INNER\s+JOIN|TRUNCATE';
		static $keywords2 = 'ALL|DISTINCT|DISTINCTROW|IGNORE|AS|USING|ON|AND|OR|IN|IS|NOT|NULL|[RI]?LIKE|REGEXP|TRUE|FALSE';

		// insert new lines
		$sql = " $sql ";
		$sql = preg_replace("#(?<=[\\s,(])($keywords1)(?=[\\s,)])#i", "\n\$1", $sql);

		// reduce spaces
		$sql = preg_replace('#[ \t]{2,}#', ' ', $sql);

		$sql = wordwrap($sql, 100);
		$sql = preg_replace('#([ \t]*\r?\n){2,}#', "\n", $sql);

		// syntax highlight
		$sql = htmlSpecialChars($sql, ENT_IGNORE, 'UTF-8');
		$sql = preg_replace_callback("#(/\\*.+?\\*/)|(\\*\\*.+?\\*\\*)|(?<=[\\s,(])($keywords1)(?=[\\s,)])|(?<=[\\s,(=])($keywords2)(?=[\\s,)=])#is", function ($matches) {
			if (!empty($matches[1])) { // comment
				return '<em style="color:gray">' . $matches[1] . '</em>';

			} elseif (!empty($matches[2])) { // error
				return '<strong style="color:red">' . $matches[2] . '</strong>';

			} elseif (!empty($matches[3])) { // most important keywords
				return '<strong style="color:blue">' . $matches[3] . '</strong>';

			} elseif (!empty($matches[4])) { // other keywords
				return '<strong style="color:green">' . $matches[4] . '</strong>';
			}
		}, $sql);

		// parameters
		$sql = preg_replace_callback('#\?#', function () use ($params, $connection) {
			static $i = 0;
			if (!isset($params[$i])) {
				return '?';
			}
			$param = $params[$i++];
			if (is_string($param) && (preg_match('#[^\x09\x0A\x0D\x20-\x7E\xA0-\x{10FFFF}]#u', $param) || preg_last_error())) {
				return '<i title="Length ' . strlen($param) . ' bytes">&lt;binary&gt;</i>';

			} elseif (is_string($param)) {
				$length = $this->length($param);
				$truncated = $this->truncate($param, $this->sql_length);
				$text = htmlspecialchars($connection ? $connection->quote($truncated) : '\'' . $truncated . '\'', ENT_NOQUOTES, 'UTF-8');
				return '<span title="Length ' . $length . ' characters">' . $text . '</span>';

			} elseif (is_resource($param)) {
				$type = get_resource_type($param);
				if ($type === 'stream') {
					$info = stream_get_meta_data($param);
				}
				return '<i' . (isset($info['uri']) ? ' title="' . htmlspecialchars($info['uri'], ENT_NOQUOTES, 'UTF-8') . '"' : NULL)
					. '>&lt;' . htmlSpecialChars($type, ENT_NOQUOTES, 'UTF-8') . ' resource&gt;</i> ';

			} else {
				return htmlspecialchars($param, ENT_NOQUOTES, 'UTF-8');
			}
		}, $sql);

		return '<pre class="dump">' . trim($sql) . "</pre>\n";
	}	
	
	private function length($s)
	{
		return function_exists('mb_strlen') ? mb_strlen($s, 'UTF-8') : strlen(utf8_decode($s));
	}
	
	/**
	 * Truncates string to maximal length.
	 * @param  string  UTF-8 encoding
	 * @param  int
	 * @param  string  UTF-8 encoding
	 * @return string
	 */
	public static function truncate($s, $maxLen, $append = "\xE2\x80\xA6")
	{
		if (self::length($s) > $maxLen) {
			$maxLen = $maxLen - self::length($append);
			if ($maxLen < 1) {
				return $append;

			} elseif ($matches = self::match($s, '#^.{1,'.$maxLen.'}(?=[\s\x00-/:-@\[-`{-~])#us')) {
				return $matches[0] . $append;

			} else {
				return self::substring($s, 0, $maxLen) . $append;
			}
		}
		return $s;
	}

	// <a class=\"nette-DbConnectionPanel-source" href="editor://open/?file=%2FUsers%2Fvojtechlacina%2FGit%2FKOMINY-info%2Ftemp%2Fcache%2Flatte%2Fmodules-FrontModule-templates-Stock-products-latte-Templatef942049cd6ed151787eb9be25a2afa41.php&amp;line=31" title="/Users/vojtechlacina/Git/KOMINY-info/temp/cache/latte/modules-FrontModule-templates-Stock-products-latte-Templatef942049cd6ed151787eb9be25a2afa41.php:31">.../temp/cache/latte/<b>modules-FrontModule-templates-Stock-products-latte-Templatef942049cd6ed151787eb9be25a2afa41.php</b>:31</a>
}