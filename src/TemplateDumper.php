<?php

namespace Redenge;

/**
 * Debugování templatů.
 * @todo Seznam definovaných bloků + proměných.
 * Zobrazit strukturu je-li to možné.
 * Automatická detekce remote (nahrazeni app dir).
 */
final class TemplateDumper implements Tracy\IBarPanel
{
	private $data;

	private $count;

	private $editor;

	private $replaceAppDir;


	public function __construct($data, $editor = NULL, $replaceAppDir = NULL)
	{
		$this->data = $data;
		$this->editor = $editor;
		$this->count = count($data);

		if ($replaceAppDir) {
			$this->replaceAppDir = (object) $replaceAppDir;
		} else {
			// Automatická detekce a nahrazení
			// ...
			// isset($_SERVER['REMOTE_ADDR']) $_SERVER['SERVER_ADDR']
		}
	}


	/**
	 * Přemapuje cestu k souborům pro otevření v editoru.
	 */
	public function replaceAppDir($original, $new = FALSE)
	{
		if ($new) {
			$original = [
				'original' => $original,
				'new' => $new,
			];
		}

		$this->replaceAppDir = (object) $original;
	}


	public function getTab()
	{
		return '<span title="FU*K TemplatePower">'.
			'<svg version=\"1.1\" width="16" height="16" viewBox="0 0 16 16">'.
			'<path fill="#543071" d="M13.538 1.332c-0.559-0.308-2.1-1.067-3.105-1.276-1.044-0.217-2.598 3.86-3.092 3.878-0.801 0.029-0.097-2.784 0.664-3.935-1.188 0.793-3.299 1.858-3.793 3.083-0.955 2.367 1.769 6.628 0.316 6.873-0.453 0.076-1.708-2.938-2.034-3.858-0.921 1.76-0.228 3.407 0.534 5.465v0.128c-0.029 0.111-0.054 0.218-0.081 0.326h0.081v2.987c0 0.55 0.445 0.996 0.996 0.996s0.996-0.445 0.996-0.996v-3.018c1.858-0.209 2.732 0.4 4.005-2.013-0.777 0.321-2.328-0.658-2.006-1.12 0.647-0.93 3.29 0.053 4.975-2.807-2.667 0.711-2.982-0.308-3.007-0.68-0.053-0.769 2.666-0.362 3.905-1.431 0.719-0.62 1.58-2.088 0.647-2.604z"></path>'.
			'</svg>' . $this->count . '</span>';
	}


	public function getPanel()
	{
		$html = '<h1 title="">Loaded templates: ' . $this->count . '</h1>';

		if ($this->replaceAppDir) {
			$html .= "<h1 title=\"{$this->replaceAppDir->original} => {$this->replaceAppDir->new}\">\$replaceAppDir: On</h1>";
		} else {
			$html .= '<h1>$replaceAppDir: Off</h1>';
		}

		$html .= "<div class=\"tracy-inner nette-DbConnectionPanel\">
	<table>
		<tbody>
			<tr>
				<th>Template</th>
				<th>From</th>
			</tr>";

		foreach ($this->data as $tpl) {

			$template = str_replace('%line', 0, str_replace('%file', $tpl->path, $this->editor));
			$source = str_replace('%line', $tpl->line, str_replace('%file', $tpl->sourcePath, $this->editor));

			if ($this->replaceAppDir) {
				$template = str_replace($this->replaceAppDir->original, $this->replaceAppDir->new, $template);
				$source = str_replace(realpath($this->replaceAppDir->original), $this->replaceAppDir->new, $source);
			}

			$html .= "<tr><td><a href=\"{$template}\" style=\"color: #543071; font-weight: bold\">{$tpl->name}</a></td>".
				"<td><strong><a href=\"{$source}\">{$tpl->sourceName}</strong>:{$tpl->line}</a></td></tr>";
		}

		$html .= "</tbody></table></div>";

		return $html;
	}

}