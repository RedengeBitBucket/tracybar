<?php

namespace Redenge;
/**
 * Dumper object
 * Singleton
 */
class Dumper //extends Nette\Object implements IServiceLocator
{
	private static $instance = FALSE;

	protected $data = array();


	private function __construct() {}


	private function __clone() {}


	private function __wakeup() {}


	public static function getInstance()
	{
		if (self::$instance === FALSE)
			self::$instance = new Dumper;

		return self::$instance;
	}


	public static function add($data, $type = 'default')
	{
		$dumper = Dumper::getInstance();
		$dumper->data[$type][] = $data;
	}


	public static function get($type = 'default')
	{
		$dumper = Dumper::getInstance();
		return isset($dumper->data[$type]) ? $dumper->data[$type] : array();
	}

}
